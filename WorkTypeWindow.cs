﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;

namespace Softech.Softime
{
    public partial class WorkTypeWindow : Form
    {
        Datalayer datalayer = null;
        GridViewRowInfo currentRow;
        List<EntryStatus> dirtyList = new List<EntryStatus>();

        int UserId;
        bool justAdded = false;

        public WorkTypeWindow()
        {
            InitializeComponent();
        }

        public WorkTypeWindow(Datalayer dl, int userId)
            : this()
        {
            datalayer = dl;
            UserId = userId;
        }

        private void WorkTypeWindow_Load(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            gridWorktype.DataSource = datalayer.GetSifrantiVrst();
            gridWorktype.BestFitColumns(Telerik.WinControls.UI.BestFitColumnMode.DisplayedCells);
            gridWorktype.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;

            var articleColumn = gridWorktype.GetColumn<GridViewMultiComboBoxColumn>("acIdent");
            articleColumn.DataSource = datalayer.GetArticles();
        }

        private void btnDeleteEntry_Click(object sender, EventArgs e)
        {
            var row = gridWorktype.CurrentRow;
            if (row == currentRow)
            {
                dirtyList.Remove(dirtyList.Where(le => le.RowIndex == row.Index).SingleOrDefault());
                gridWorktype.Rows.Remove(row);
                currentRow = null;
                return;
            }

            if (row != null)
            {
                string id = row.Cells["acSetOf"].Value.ToString();
                DialogResult result = MessageBox.Show(
                    string.Format("Izbrišem vnos {0}?", id),
                    "Brisanje", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question
                );

                if (result == DialogResult.Yes)
                {
                    if (datalayer.DeleteWorktype(id, UserId))
                    {
                        gridWorktype.Rows.Remove(row);
                        dirtyList.Remove(dirtyList.Where(le => le.RowIndex == row.Index).SingleOrDefault());
                    }
                }
            }
        }

        private void btnNewEntry_Click(object sender, EventArgs e)
        {
            justAdded = true;
            if (Validate() || gridWorktype.Rows.Count == 0)
            {
                currentRow = gridWorktype.Rows.AddNew();
                dirtyList.Add(new EntryStatus(currentRow.Index, true));
            }
        }

        private new bool Validate()
        {
            return Validate(gridWorktype.CurrentRow);
        }

        private bool Validate(GridViewRowInfo row)
        {
            bool result = true;
            var current = row;
            if (current != null)
            {
                var ident = current.Cells["acSetOf"].Value.ToString().Trim();
                var name = current.Cells["acName"].Value.ToString().Trim();

                if (ident.Equals(string.Empty) || name.Equals(string.Empty))
                {
                    current.ErrorText = "Validacija ni uspela";
                    result = false;
                }
            }
            return result;
        }

        private bool ValidateAll()
        {
            foreach (var row in gridWorktype.Rows)
            {
                if (!Validate(row))
                    return false;
            }
            return true;
        }

        private bool SaveAll()
        {
            if (!ValidateAll())
            {
                MessageBox.Show("Nekateri vnosi so nepravilno formatirani, prosim popravite vnose.", "Napaka", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            if (dirtyList.Count == 0)
                return false;

            foreach (var element in dirtyList.ToList())
            {
                if (gridWorktype.Rows.Count == 0)
                    break;
                var row = gridWorktype.Rows[element.RowIndex];
                if (row != null)
                {
                    if (Validate(row))
                    {
                        WorkTypeEntry entry = new WorkTypeEntry();
                        entry.Identifier    = row.Cells["acSetOf"].Value.ToString();
                        entry.Name          = row.Cells["acName"].Value.ToString();
                        entry.pIdentifier   = row.Cells["acParentType"].Value.ToString();
                        entry.Article       = row.Cells["acIdent"].Value.ToString();

                        string mandatory    = row.Cells["anIsSubjMandatory"].Value.ToString() == "" ? "false" : "true";
                        entry.Mandatory     = bool.Parse(mandatory);

                        string chargeable = row.Cells["anIsChargeable"].Value.ToString() == "" ? "false" : "true";
                        entry.Chargeable  = bool.Parse(chargeable);

                        bool result = false;
                        if (element.New)
                            result = datalayer.InsertWorktype(entry, UserId);
                        else
                            result = datalayer.UpdateWorktype(entry, UserId);
                        if (result)
                            dirtyList.Remove(element);
                    }
                }
            }
            return dirtyList.Count == 0;
        }

        private bool NeedSaving()
        {
            if (gridWorktype.Rows.Count == 0)
                return false;

            return dirtyList.Count > 0;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveAll())
                MessageBox.Show("Vsi vnosi so bili shranjeni", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void WorkTypeWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (NeedSaving())
            {
                DialogResult result = MessageBox.Show("Imate neshranjene spremembe, želite shraniti?", "Neshranjene spremembe", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                    SaveAll();
                else if (result == DialogResult.Cancel)
                    e.Cancel = true;
            }
        }

        private void gridWorktype_RowsChanged(object sender, GridViewCollectionChangedEventArgs e)
        {
            if (gridWorktype.CurrentRow != null)
            {
                if (justAdded)
                {
                    justAdded = false;
                    return;
                }

                var elements = dirtyList.Where(ic => ic.RowIndex == gridWorktype.CurrentRow.Index).ToList();
                if (elements.Count == 0)
                {
                    dirtyList.Add(new EntryStatus(gridWorktype.CurrentRow.Index, false));
                    dirtyList = dirtyList.GroupBy(i => i.RowIndex).Select(y => y.First()).ToList();
                }
            }
        }

        private void gridWorktype_CellEditorInitialized(object sender, GridViewCellEventArgs e)
        {
            var element = e.ActiveEditor as RadMultiColumnComboBoxElement;
            if (element != null)
            {
                element.AutoFilter = true;
                element.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
                if (gridWorktype.Columns[e.ColumnIndex].Name == "acOperation")
                {
                    element.Columns["acSubject"].HeaderText = "ID";
                    element.Columns["acName2"].HeaderText    = "Naziv";
                }

                if (gridWorktype.Columns[e.ColumnIndex].Name == "acIdent")
                {
                    element.Columns["acIdent"].HeaderText = "ID";
                    element.Columns["acName"].HeaderText  = "Artikel";

                    FilterDescriptor filter = new FilterDescriptor();
                    filter.PropertyName = element.DisplayMember;
                    filter.Operator     = FilterOperator.Contains;
                    element.EditorControl.MasterTemplate.FilterDescriptors.Add(filter);
                }

                element.AutoSizeDropDownToBestFit = true;
            }
        }

        private void gridWorktype_CellValueChanged(object sender, GridViewCellEventArgs e)
        {
            if (gridWorktype.Columns[e.ColumnIndex].Name == "acIdent")
            {
                var element = e.ActiveEditor as RadMultiColumnComboBoxElement;
                if (element != null)
                {
                    var item = dirtyList.Where(el => el.RowIndex == e.RowIndex).SingleOrDefault();
                    if (item != null)
                    {
                        dirtyList.Add(new EntryStatus(item.RowIndex, item.New));
                        dirtyList.Remove(item);
                    }
                }
            }
        }

        private void gridWorktype_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                var current = gridWorktype.CurrentRow;
                if (current != null)
                {
                    btnDeleteEntry.PerformClick();
                }
            }
        }
    }

    public class EntryStatus
    {
        public int RowIndex;
        public bool New;

        public EntryStatus(int index, bool isNew)
        {
            RowIndex = index;
            New = isNew;
        }
    }
}
