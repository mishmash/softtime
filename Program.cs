﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;
using System.IO;

namespace Softech.Softime
{
    static class Program
    {
        /// <summary>
        /// Dovoljeni ključi
        /// </summary>
        public static readonly string[] keys = 
        {
            "998735",
            "995690"
        };

        [STAThread]
        static void Main()
        {
            bool foundValidKey = false;
            foreach (string key in keys)
            {
                if (VerifyActivated(key))
                    foundValidKey = true;
            }

            if (!foundValidKey)
            {
                MessageBox.Show(
                    "Program ni aktiviran in se bo zaprl.\n" +
                    "Prosim če pokličete podjetje SofTeh d.o.o.\n\n" + 
                    "Tel: 02 421 56 70\n" +
                    "Email: info@softeh.si",
                    string.Format("{0} ni aktiviran", 
                    Extensions.GetAssemblyTitle()), 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Exclamation
                );
                Application.Exit();
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //
            Datalayer datalayer = new Datalayer(Properties.Settings.Default.ConnectionString);
            if (!datalayer.TestConnection())
            {
                MessageBox.Show("Povezava s podatkovno bazo ni uspela.", "Napaka", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        RetryStoredProcedure:
            if (!Extensions.IsSpInstalled(datalayer.GetCurrentDatabase()) && !Extensions.InstallStoredProcedures(datalayer))
            {
                DialogResult result = MessageBox.Show("Inicializacija nekaterih SQL procedur ni uspela.", "Napaka", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
                if (result == DialogResult.Abort)
                    return;
                else if (result == DialogResult.Retry)
                    goto RetryStoredProcedure;
            }
            else
            {
                Extensions.AddSpInstalled(datalayer.GetCurrentDatabase());
            }

            LoginWindow login = new LoginWindow();
            if (login.ShowDialog() == DialogResult.OK)
            {
                Application.Run(new MainWindow((DatabaseEntry)login.Database, login.UserId, login.UserType));
            }
        }

        public static bool VerifyActivated(string key)
        {
            Datalayer datalayer = new Datalayer(Properties.Settings.Default.ConnectionString);
            if (datalayer.ConfirmKey(key))
                return true;
            else
                return false;
        }

    }
}
